 #Find first runners Up
 def solution():
    #Read in scores as a string of number
    n = int(raw_input())
    #split this string into strings of each score then convert the substrings to integer
    score_array = map(int,  raw_input().split())
    #sort the resulting integers then then output the second largest 
    print(sorted(list(set(score_array)))[-2])

solution()
